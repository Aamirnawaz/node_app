const express = require('express');
const router = express.Router();
var session = require('express-session');
var passport = require('passport');
var SqlString = require('sqlstring');
var dateFormat = require('dateformat');
var now = new Date();
const myConnection = require('../dbConnection');
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
var flash = require('connect-flash');
var moment = require('moment');
moment().format();

//signup get router
router.get('/signup',function (req,res,next) {
   res.render('signup',{title:"Registeration Page",errors:false,success:false});
});

//signup post router
router.post('/signup',function (req,res,next) {
   req.checkBody('name','Name Cant be empty').notEmpty();
   req.checkBody('username','username cant be empty').notEmpty();
   req.checkBody('username','Username must be 5-10 cherectors long').len(5,10);
   req.checkBody('password','password contain mix charectors').notEmpty();
   req.checkBody('password','password contains 5-10 charactors long').len(5,10);
    req.checkBody('Confirmpassword','password contains 5-10 charactors long').len(5,10);
   req.checkBody('Confirmpassword','password did not match with new password').equals(req.body.password);

   const errors = req.validationErrors();
   if(errors){
       res.render('signup',{title:"Registration Page",errors:errors,success:''});
   }else {
       bcrypt.hash(req.body.password, 8, function(err, hash) {
           var password = hash;
           var post_data ={
               name:req.body.name,
               username:req.body.username,
               email:req.body.email,
               password:password
           }
           var sql = SqlString.format("INSERT INTO user SET ?",post_data);
           myConnection.query(sql, function (err, result) {
               if (err) throw err;

               myConnection.query('SELECT LAST_INSERT_ID() as user_id',function (error,results,fields) {
                  if(error) throw error;
                  const user_id = results[0];
               });

               res.render('signup',{title:"Successfully Registered!",errors:'',success:"Registration Completed"});
           });
       });
   }
});

//passport serializeUser
passport.serializeUser(function(user_id, done) {
    done(null,user_id);
});

//passport deserializeUser
passport.deserializeUser(function(user_id, done) {
    done(null, user_id);
});

//login view route
router.get('/',function (req,res,next) {

    res.render('login',{title:'Login Form'});
    ;
});

//login validate
router.post('/login',passport.authenticate ('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/login',
        failureFlash: true,
        successFlash: 'You have logged in'
    })
);

//logout user
router.get('/logout',function (req,res,next) {
    req.session.destroy();
    res.redirect('/login');
});

//dashboard route
router.get('/dashboard',IsAuthenticated,function (req,res,next) {
    //multiple queries statment   ('select query1;select query2;select query3'); result[0],result[1],result[2];
    var countQuery ="select count(*) As ordersCount from orders;select count(*) As pendingOrders from orders where order_status='pending';select count(*) As processOrders from orders where order_status='process';select count(*) As deliveredOrders from orders where order_status='delivered';select count(*) As rejectedOrders from orders where order_status='rejected';select * from orders where order_status='pending';select * from orders where order_status='process';select * from orders where order_status='delivered';select * from orders where order_status='rejected'";

    myConnection.query(countQuery,function (error,results,fields) {
        if (error) throw error;

        // console.log(results[5]);
        res.render('admin/dashboard',{
            orders_count:results[0],
            pendingOrders:results[1],
            processOrders:results[2],
            deliveredOrders:results[3],
            rejectedOrders:results[4],
            pendingOrdersData:results[5],
            processOrdersData:results[6],
            deliveredOrdersData:results[7],
            rejectedOrdersData:results[8],
        });
    });
})

//orders routes
router.get('/orders',IsAuthenticated,function (req,res,next) {
    myConnection.query('select * from orders',function (error,rows,fields) {
        if(error) throw error;
        res.render('admin/orders',{data:rows,successMessage:false});
    });
});

//add_order route
router.get('/add_order',IsAuthenticated,function (req,res,next) {
    res.render('admin/add_order',{order_data:false,order_id:''});
})

//save order
router.post('/save_order',IsAuthenticated,function (req,res,next) {
    let fname = req.body.first_name;
    let contact_number = req.body.contact_number;
    let address = req.body.address;
    let email = req.body.email;
    let cuff = req.body.cuff;
    let collar = req.body.collar;
    let chest = req.body.chest;
    let waist = req.body.waist;
    let seat = req.body.seat;
    let front_width = req.body.front_width;
    let sleev_width = req.body.sleev_width;
    let short_sleev_length = req.body.short_sleev_length;
    let short_sleev_opening = req.body.short_sleev_opening;
    let biceps = req.body.biceps;
    let armhole = req.body.armhole;
    let shoulder = req.body.shoulder;
    let back_width = req.body.back_width;
    let shirt_length_front = req.body.shirt_length_front;
    let shirt_length_back = req.body.shirt_length_back;
    let delivery_date = req.body.delivery_date;
    let gender = req.body.gender;
    console.log(delivery_date);

    var  post_data = {
        fname:fname,
        contact : contact_number,
        address : address,
        cuff : cuff,
        collar : collar,
        chest : chest,
        waist : waist,
        seat : seat,
        front_width :front_width ,
        sleev_width :sleev_width,
        short_sleev_length :short_sleev_length ,
        short_sleev_opening : short_sleev_opening,
        biceps : biceps,
        armhole : armhole,
        shoulder :shoulder ,
        back_width : back_width,
        shirt_length_front : shirt_length_front,
        shirt_length_back :shirt_length_back,
        delivery_date : delivery_date.toLocaleString('en-us', { year:'numeric',month: 'numeric',day:'numeric'}),

        gender:gender
    };

    var sql = SqlString.format("INSERT INTO orders SET ?",post_data);
    // console.log(sql);
    myConnection.query(sql, function (err, result) {
        if (err) throw err;
        // console.log("1 record inserted");
        res.redirect('/orders');
    });
});

//edit order
router.get('/edit_order/:order_id',IsAuthenticated,function (req,res,next) {
    //array based orderId
    var orderId = req.params;
    //object based
    var order_id = orderId.order_id;

    var countQuery =(`select * from orders where order_id=${order_id}`);
    myConnection.query(countQuery,function (error,results,fields) {
        if (error) throw error;
        // console.log(results);
        res.render('admin/add_order',{order_id:order_id,order_data:results});
    });

});

//update order
router.post('/update_order/:order_id',IsAuthenticated,function (req,res,next) {

    var order_id = req.params.order_id;
    // console.log(req.body);


    var  post_data = {
        fname:req.body.fname,
        contact : req.body.contact_number,
        address : req.body.address,
        email : req.body.email,
        cuff : req.body.cuff,
        collar : req.body.collar,
        chest : req.body.chest,
        waist : req.body.waist,
        seat : req.body.seat,
        front_width :req.body.front_width ,
        sleev_width :req.body.sleev_width,
        short_sleev_length :req.body.short_sleev_length ,
        short_sleev_opening :req.body.short_sleev_opening,
        biceps : req.body.biceps,
        armhole : req.body.armhole,
        shoulder :req.body.shoulder ,
        back_width : req.body.back_width,
        shirt_length_front : req.body.shirt_length_front,
        shirt_length_back :req.body.shirt_length_back,
        delivery_date : req.body.delivery_date.toLocaleString('en-us', { year:'numeric',month: 'numeric',day:'numeric'}),

        gender:req.body.gender
    };


    var sql = SqlString.format('update orders  SET ? where order_id='+order_id,post_data);
    console.log(sql);
    // return false;

    myConnection.query(sql, function (err, result) {
        if (err) throw err;
        // console.log("1 record inserted");
        res.redirect('/orders');
    });


});

//delete order
router.get('/delete_order/:order_id',IsAuthenticated,function (req,res,next) {
    var order_id = req.params.order_id;
    var query = `delete FROM orders where order_id=${order_id}`;
    myConnection.query(query,function (error,results,fields) {
        if(error) throw error;
        res.redirect('/orders');
    });
});

//track spefic order view
router.get('/track_order',IsAuthenticated,function (req,res,next) {
   res.render('admin/track_order');
});

router.post('/track_order',IsAuthenticated,function (req,res,next) {

    var order_name = req.body.response_data;

    // var order_id = req.body.response.data;
    var query = `select * FROM orders where fname like '${order_name}%'`;
    console.log(query);
    myConnection.query(query,function (error,results,fields) {
        if(error) throw error;
        // res.redirect('/orders');
        console.log(results);
        // console.log(results[0].fname);
        res.json(results);
    });


});



//changeTopending
router.get('/changeTopending/:order_id',IsAuthenticated,function (req,res,next) {
        //array based orderId
        var orderId = req.params;
        //object based
        var order_id = orderId.order_id;
        var sql =SqlString.format(`update orders set order_status='pending' where order_id=${order_id}`);
        console.log(sql);

        myConnection.query(sql,function (error,result,feilds) {
           if(error) {
               throw error;
           }else{
               res.redirect('/orders');
           }
        });
});

//changeToProcess
router.get('/changeToProcess/:order_id',IsAuthenticated,function (req,res,next) {
    //array based orderId
    var orderId = req.params;
    //object based
    var order_id = orderId.order_id;
    var sql =SqlString.format(`update orders set order_status='process' where order_id=${order_id}`);
    console.log(sql);

    myConnection.query(sql,function (error,result,feilds) {
        if(error) {
            throw error;
        }else{
            res.redirect('/orders');
        }
    });
});

//changeToDeliverd
router.get('/changeToDeliverd/:order_id',IsAuthenticated,function (req,res,next) {
    //array based orderId
    var orderId = req.params;
    //object based
    var order_id = orderId.order_id;
    var sql =SqlString.format(`update orders set order_status='delivered' where order_id=${order_id}`);
    console.log(sql);

    myConnection.query(sql,function (error,result,feilds) {
        if(error) {
            throw error;
        }else{
            res.redirect('/orders');
        }
    });
});

//changeToRejected
router.get('/changeToRejected/:order_id',IsAuthenticated,function (req,res,next) {
    //array based orderId
    var orderId = req.params;
    //object based
    var order_id = orderId.order_id;
    var sql =SqlString.format(`update orders set order_status='rejected' where order_id=${order_id}`);
    console.log(sql);

    myConnection.query(sql,function (error,result,feilds) {
        if(error) {
            throw error;
        }else{
            res.redirect('/orders');
        }
    });
});

//User authentication function
function IsAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        next();
    }else{
        // next(new Error(401));
        res.redirect('/login');
    }
}


module.exports = router;
