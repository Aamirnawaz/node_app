const mysql = require('mysql');

const myConnection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'smarttailor',
    multipleStatements: true,
});

myConnection.connect();

module.exports = myConnection;