-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2018 at 07:42 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smarttailor`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(60) NOT NULL,
  `address` varchar(60) NOT NULL,
  `contact` varchar(60) NOT NULL,
  `delivery_date` date NOT NULL,
  `collar` varchar(10) NOT NULL,
  `waist` varchar(10) NOT NULL,
  `seat` varchar(10) NOT NULL,
  `front_width` varchar(10) NOT NULL,
  `sleev_width` varchar(10) NOT NULL,
  `short_sleev_length` varchar(10) NOT NULL,
  `short_sleev_opening` varchar(10) NOT NULL,
  `biceps` varchar(10) NOT NULL,
  `armhole` varchar(10) NOT NULL,
  `shoulder` varchar(10) NOT NULL,
  `back_width` varchar(10) NOT NULL,
  `shirt_length_front` varchar(10) NOT NULL,
  `shirt_length_back` varchar(10) NOT NULL,
  `cuff` varchar(10) NOT NULL,
  `chest` varchar(10) NOT NULL,
  `order_placed_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_status` enum('pending','delivered','process','rejected') NOT NULL,
  `process_date` datetime NOT NULL,
  `rejected_date` datetime NOT NULL,
  `delivery_time` datetime NOT NULL,
  `is_order_deleted` int(1) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `fname`, `address`, `contact`, `delivery_date`, `collar`, `waist`, `seat`, `front_width`, `sleev_width`, `short_sleev_length`, `short_sleev_opening`, `biceps`, `armhole`, `shoulder`, `back_width`, `shirt_length_front`, `shirt_length_back`, `cuff`, `chest`, `order_placed_date`, `order_update_date`, `order_status`, `process_date`, `rejected_date`, `delivery_time`, `is_order_deleted`) VALUES
(2, 'Aamir', 'kohat pakistan\r\n                                            ', '09343434344', '0000-00-00', '23', '3', '32', '43', '34', '34', '34', '34', '34', '34', '34', '34', '34', '112', '32', '2018-12-29 08:48:50', '0000-00-00 00:00:00', 'delivered', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 'Aamir', 'kohat pakistan\r\n                                            ', '09343434344', '0000-00-00', '23', '3', '32', '43', '34', '34', '34', '34', '34', '34', '34', '34', '34', '112', '32', '2018-12-29 08:52:37', '0000-00-00 00:00:00', 'process', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 'tariq', 'fdkfjksf\r\n                                                ', '234324343434', '0000-00-00', '234', '234', '213', '1323', '33434', '234', '324', '324', '324', '34', '234', '234', '324', '234', '2354', '2018-12-28 20:26:18', '0000-00-00 00:00:00', 'pending', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'sdf', '\n                                                efwedf', '3234322434', '0000-00-00', '34', '34', '34', '324', '34', '324', '324', '324', '4', '324', '234', '324', '324', '2334', '34', '2018-12-29 08:52:42', '0000-00-00 00:00:00', 'rejected', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `expires`, `data`) VALUES
('cH2Ax0-rH-jcn5cMbVgUMjTLGDih7REo', 1546281716, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"}}');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_deleted` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `username`, `email`, `password`, `is_deleted`, `status`) VALUES
(4, 'admin', 'admin', 'admin@gmail.com', '$2a$08$NZuVHTkF1.yDSkTPtbkz5OqpsA0q133X05Kkfhc9TshtvsmwpbY8a', 0, 0),
(7, 'Aamir', 'aamirkhan', 'aamir@gmail.com', '$2a$08$H7.sdiRQgebuqNFJlCBJy.WX4LuI2dTjnSOW9X/61q748agpzk5Dm', 0, 0),
(8, 'tariq', 'tariq', 'tariq@gmail.com', '$2a$08$f1WYgVX205LTQT8XJjteru0lGc10T2Il4xXKUuEI1mvUEAgHzgKIS', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
