var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var expressValidator = require('express-validator');
var session = require('express-session');
var passport = require('passport');
var MySQLStore = require('express-mysql-session')(session);
var LocalStrategy = require('passport-local').Strategy;
const myConnection = require('./dbConnection');
var bcrypt = require('bcryptjs');
var flash = require('connect-flash');
let date = require('date-and-time');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var homeRouter = require('./routes/HomeController');



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var options = {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'smarttailor',
};
var sessionStore = new MySQLStore(options);

app.use(session({
    secret: 'SuperSecreteKeyWords',
    resave: true,
    store: sessionStore,
    saveUninitialized: true,
    // cookie: { secure: true } only use with https
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(expressValidator());



app.use('/', homeRouter);
app.use('/users', usersRouter);
app.use('/login',homeRouter);
// app.use('/login_check',homeRouter);

app.use(function (req,res,next) {
    req.locals.isAuthenticated=req.isAuthenticated();
    next();
});

passport.use(new LocalStrategy(
    function(username, password, done) {

        myConnection.query('select user_id,password from user where username=? ',[username],function (error,result,fields) {
            if(error){
                throw error;
            } else{
                if(result.length ===0){
                    done(null,false);
                }else {
                    const hash = result[0].password.toString();
                    bcrypt.compare(password,hash,function (err,response) {
                        if(response===true){
                            return done(null,{user_id:result[0].user_id});
                        }else{

                            return done(null,false);
                        }
                    });
                }
            }
        });
    }
));





// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
